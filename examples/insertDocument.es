POST heroes/_doc/1
{
  "name": "Spiderman",
  "power": 100,
  "health": 100,
  "team": "marvel",
  "abilities": [
    {
      "name": "spider instinct",
      "description": "bla bla bla"
    }
  ]
}

POST heroes/_doc/2
{
  "name": "Superman",
  "power": 100,
  "health": 100,
  "team": "dc",
  "abilities": [
    {
      "name": "eye laser",
      "description": "bla bla bla"
    }
  ]
}

POST heroes/_doc/3
{
  "name": "Hulk",
  "power": 100,
  "health": 100,
  "team": "marvel",
  "abilities": [
    {
      "name": "Power Fist",
      "description": "bla bla bla"
    }
  ]
}
