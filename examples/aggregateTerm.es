GET heroes/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "exists": {
            "field": "team"
          }
        }
      ]
    }
  },
  "aggs": {
    "marvel_team": {
      "terms": {
        "field": "team",
        "size": 10
      }
    }
  }
}

# futher documetation 
# https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html