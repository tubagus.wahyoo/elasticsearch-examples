PUT /heroes
{
  "settings": {
    "number_of_shards": 1,
    "analysis": {
      "filter": {
        "autocomplete_filter": {
          "type": "edge_ngram",
          "min_gram": 1,
          "max_gram": 20
        }
      },
      "analyzer": {
        "autocomplete": {
          "type": "custom",
          "tokenizer": "standard",
          "filter": [
            "lowercase",
            "autocomplete_filter"
          ]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "name": {
        "type": "text",
        "analyzer": "autocomplete",
        "search_analyzer": "standard"
      },
      "power": {
        "type": "integer"
      },
      "health": {
        "type": "integer"
      },
      "team": {
        "type": "keyword"
      },
      "abilities": {
        "type": "nested",
        "dynamic": "false",
        "properties": {
          "name": {
            "type": "text"
          },
          "description": {
            "type": "text"
          }
        }
      }
    }
  }
}

# doc normalizer
# https://www.elastic.co/guide/en/elasticsearch/reference/current/normalizer.html