GET heroes/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "exists": {
            "field": "power"
          }
        }
      ]
    }
  },
  "aggs": {
    "super_power": {
      "filter": {
        "range": {
          "power": {
            "gte": 70,
            "lte": 100
          }
        }
      }
    }
  }
}

# futher documetation 
# https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html