POST _bulk
{"update":{"_index":"heroes","_id":"4"}}
{"doc":{"power":80,"health":70}}
{"update":{"_index":"heroes","_id":"5"}}
{"doc":{"power":90,"health":70}}

# more documentation 
# https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html
