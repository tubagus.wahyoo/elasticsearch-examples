
# Create data sample geo polygon
PUT /user_location
{
  "mappings": {
    "properties": {
      "name": {
        "type": "text"
      },
      "location": {
        "type": "geo_point"
      }
    }
  }
}

POST user_location/_doc/1
{
  "name": "William",
  "location": "-25.443053, -49.238396"
}

POST user_location/_doc/2
{
  "name": "Robert",
  "location": "-25.440173, -49.243169"
}

POST user_location/_doc/3
{
  "name": "Bernard",
  "location": "-25.440262, -49.247720"
}

GET user_location/_search
{
  "query": {
    "bool" : {
      "filter" : {
        "geo_distance" : {
          "distance" : "300m",
          "location" : "-25.442987, -49.239504"
        }
      }
    }
  }
}


# QUERY GEO POLYGON
GET user_location/_search
{
  "query": {
    "bool" : {
      "filter" : {
         "geo_polygon" : {
            "location" : {
              "points" : [
                "-25.44373,-49.24248",
                "-25.44297,-49.24230",
                "-25.44177,-49.23642",
                "-25.43961,-49.23822",
                "-25.43991,-49.23781",
                "-25.44170,-49.23647",
                "-25.44210,-49.23586",
                "-25.44218,-49.23506",
                "-25.44358,-49.23491",
                "-25.44406,-49.24139",
                "-25.44373,-49.24248"
              ]
            }
        }
      }
    }
  }
}