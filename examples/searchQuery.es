GET heroes/_search?q=iron

GET heroes/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "name": "man"
          }
        }
      ],
      "filter": [
        {
          "term": {
            "team": "marvel"
          }
        }
      ]
    }
  }
}
