POST _bulk
{"index":{"_index":"heroes","_id":"4"}}
{"name":"Iron Man","power":100,"health":100,"team":"marvel","abilities":[{"name":"Missile","description":"bla bla bla"}]}
{"index":{"_index":"heroes","_id":"5"}}
{"name":"Thor","power":100,"health":100,"team":"marvel","abilities":[{"name":"Thunder","description":"bla bla bla"}]}

# more documentation 
# https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html
