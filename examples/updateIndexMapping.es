PUT /heroes/_mapping
{
  "properties": {
      "name": {
        "type": "text",
        "analyzer": "autocomplete",
        "search_analyzer": "standard"
      },
      "power": {
        "type": "integer"
      },
      "health": {
        "type": "integer"
      },
      "team":{
        "type": "keyword"
      },
      "abilities": {
        "type": "nested",
        "dynamic": "false",
        "properties": {
          "name": {
            "type": "text"
          },
          "description": {
            "type": "text"
          }
        }
      }
  }
}