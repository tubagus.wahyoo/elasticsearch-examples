#bash
mkdir -p ~/docker-containers/elasticsearch
mkdir -p ~/docker-containers/kibana
cp kibana.yml ~/docker-containers/kibana/kibana.yml
cp elasticsearch.yml ~/docker-containers/elasticsearch/elasticsearch.yml
docker-compose -f docker-compose.yml up -d